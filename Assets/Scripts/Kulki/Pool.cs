﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class Pool : MonoBehaviour {


	public List<GameObject>list;

	public List<GameObject>ActiveList{
		get {
			return list.Where((x) => x.activeInHierarchy).ToList();
		}
	}
	public GameObject prefab;

	public void Init(int size){
		list = new List<GameObject>();
		for(int i = 0; i < size; i++){
			GameObject obj = (GameObject)Instantiate(prefab);
			obj.SetActive (false);
			list.Add(obj);
		}
	}

	public GameObject GetObject(){
		for(int i = list.Count - 1; i>0;i--){
			GameObject obj = list [i];
			if (!obj.activeSelf) {
				return obj;
			}
		}
			return null;

	}

	public void DestroyObjectPoll(GameObject obj){

		obj.SetActive(false);
	}


	public void ClearPool(){
		for(int i = list.Count - 1; i>0;i--){
			GameObject obj = list [i];
			list.RemoveAt (i);
			Destroy (obj);
		}
		list = null;
	}
	void Awake(){
		Init (500);
	}
	/*void Update(){
		for(int i = list.Count - 1; i>0;i--){
			GameObject obj = list [i];
			if (obj.transform.position.magnitude > 12) {
				DestroyObjectPoll (obj);
			}
			//Destroy (obj);
		}
	}*/
}
