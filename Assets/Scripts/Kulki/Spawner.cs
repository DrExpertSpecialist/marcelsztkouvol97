﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Spawner : MonoBehaviour {
	private bool inverted = false;
	public Pool pool;
	public Text counter;
	float Timer = 0,count=0;
	// Use this for initialization
	void Start () {
		pool.Init(250);
	}
	
	// Update is called once per frame
	void Update () {
		counter.text = ""+count;
		Timer += Time.deltaTime;
		if(Timer > 0.25 && count < 250){
			Timer -= 0.25f;
			count++;
			GameObject newSphere = pool.GetObject();

			newSphere.transform.position = new Vector3(Random.Range(-15, 15),Random.Range(-15, 15), Random.Range(-3, 13));
			newSphere.SetActive(true);
			newSphere.GetComponent<gravity>().spawner = this;
		}else{
			if(pool.ActiveList.Count >= 250 && !inverted){
				foreach(GameObject sphere in pool.ActiveList){
					if(sphere.activeInHierarchy)sphere.GetComponent<gravity>().repell = true;
				}
				inverted = true;
			}
		}
		
	}
	public void Explode(int quantity, Vector3 position){
		for(int i=0;i<quantity;i++){
			GameObject newSphere = pool.GetObject();
			if(newSphere!=null){
				newSphere.transform.position = position;
				newSphere.GetComponent<gravity>().CooldownAndBoost(this);
			}
		}
	}
}
