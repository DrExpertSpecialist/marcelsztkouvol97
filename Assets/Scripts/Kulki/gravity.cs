﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gravity : MonoBehaviour {
	public Spawner spawner;
	public float mass = 1, timer = 0;
	public Rigidbody rb;
	public bool repell = false, interact = true;

	// Use this for initialization
	void Start () {
		mass = 1;
		rb = gameObject.GetComponent<Rigidbody> ();
	}

	// Update is called once per frame
	void FixedUpdate () {
		if (spawner != null && spawner.pool.ActiveList.Count > 1) {
			Vector3 pull = Vector3.zero;
			foreach (GameObject sphere in spawner.pool.ActiveList) {
				if (this.transform.position != sphere.transform.position && sphere.activeInHierarchy) {
					pull -= (this.transform.position - sphere.transform.position).normalized * ((sphere.GetComponent<gravity> ().mass * mass) / (this.transform.position - sphere.transform.position).sqrMagnitude);
				}

			}
			rb.AddForce (repell? - pull : pull);
		}
		if (mass >= 50) {
			gameObject.SetActive (false);
			spawner.Explode ((int) mass, gameObject.transform.position);
		}
		if (!interact) {
			timer -= Time.fixedDeltaTime;
			if (timer <= 0) {
				interact = true;
				GetComponent<Collider> ().enabled = true;
			}
		}

	}

	public void CooldownAndBoost (Spawner spawner) {
		this.spawner = spawner;
		mass = 1;
		interact = false;
		float scale = 1 + ((mass - 1) / 5);
		transform.localScale = new Vector3 (scale, scale, scale);
		rb.mass = mass;
		timer = 0.5f;

		GetComponent<Collider> ().enabled = false;

		gameObject.SetActive (true);
		rb.AddForce (new Vector3 (Random.Range (-1, 1), Random.Range (-1, 1), Random.Range (-1, 1)).normalized *5);
	}
	void OnCollisionEnter (Collision other) {
		float otherMass = other.gameObject.GetComponent<gravity> ().mass;
		if (!repell) {
			if (otherMass > mass) {
				gameObject.SetActive (false);
			} else {
				mass += otherMass;
				float scale = 1 + ((mass - 1) / 5);
				transform.localScale = new Vector3 (scale, scale, scale);
				rb.mass = mass;
			}
		}
	}

}