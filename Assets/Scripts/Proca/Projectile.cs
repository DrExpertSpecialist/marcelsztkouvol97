using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Projectile : MonoBehaviour {

	public Rigidbody2D rb;
	public Rigidbody2D slingShot;
	public float maxDistance = 2f;
	public float release = .2f;

	public Camera camera;


	public GameObject next;

	private bool isDragged = false;

	void Update ()
	{
		if (isDragged)
		{
			Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

			if (Vector3.Distance(mousePos, slingShot.position) > maxDistance)
				rb.position = slingShot.position + (mousePos - slingShot.position).normalized * maxDistance;
			else
				rb.position = mousePos;
		}
	}

	void OnMouseDown ()
	{
		isDragged = true;
		rb.isKinematic = true;
	}

	void OnMouseUp ()
	{
		isDragged = false;
		rb.isKinematic = false;

		StartCoroutine(Release());
	}

	IEnumerator Release ()
	{
		camera.GetComponent<CameraScript>().player = gameObject;
		yield return new WaitForSeconds(release);

		GetComponent<SpringJoint2D>().enabled = false;
		this.enabled = false;

		yield return new WaitForSeconds(1f);

		if (next != null)
		{
			camera.GetComponent<CameraScript>().player = slingShot.gameObject;
			GameObject nextBall2 = Instantiate(next,next.transform.position,Quaternion.identity);
			next.GetComponent<Projectile>().next = nextBall2;
			next.SetActive(true);
		}
	
	}

}
